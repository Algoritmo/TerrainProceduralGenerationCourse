﻿using System.Collections;
using UnityEngine;

namespace EndlessTerrainGeneration {

    public class EndlessTerrainGenerator : MonoBehaviour {
        #region Variables
        #region Public variables
        public GameObject plane;
        public GameObject player;
        #endregion

        #region SerializedFields
        [SerializeField]
        private bool isAutoupdateAllowed;
        #endregion

        #region Private variables
        private int planeSize = 10;
        private int halfTilesSizeX = 10;
        private int halfTilesSizeZ = 10;

        private Vector3 startPosition;

        private Hashtable tiles = new Hashtable();
        #endregion


        #region Auxiliar variables
        private float auxUpdateTime;
        private Vector3 auxPosition;

        private GameObject auxTileObject;

        private string auxTileName;

        private Tile auxTile;

        private int xMove;
        private int zMove;
        private int playerX;
        private int playerZ;

        private Hashtable auxTerrain;
        #endregion
        #endregion


        #region MonoBehaviour
        private void Start() {
            transform.position = Vector3.zero;
            startPosition = Vector3.zero;

            auxUpdateTime = Time.realtimeSinceStartup;

            for (int x = -halfTilesSizeX; x < halfTilesSizeX; x++) {
                for (int z = -halfTilesSizeZ; z < halfTilesSizeZ; z++) {
                    auxPosition = new Vector3((x * planeSize + startPosition.x),
                                            0,
                                            (z * planeSize + startPosition.z));

                    auxTileObject = Instantiate(plane, auxPosition, Quaternion.identity);

                    auxTileName = auxTileName = GenerateTileName( (int)auxPosition.x, (int)auxPosition.z );

                    auxTileObject.name = auxTileName;
                    tiles.Add( auxTileName, new Tile(auxTileObject, auxUpdateTime ) );
                }
            }
        }


        private void Update() {
            if( isAutoupdateAllowed )
                UpdateTerrain( player.transform.position.x, player.transform.position.z );
        }
        #endregion


        #region Public methods
        public void UpdateTerrain( float _xValue, float _zValue ) {
            // determine how far the player has moved since the last terrain update
            xMove = (int)( _xValue - startPosition.x );
            zMove = (int)( _zValue - startPosition.z);

            if (Mathf.Abs(xMove) >= planeSize || Mathf.Abs(zMove) >= planeSize) {
                auxUpdateTime = Time.realtimeSinceStartup;

                // force integer position and round to nearest tilesize
                playerX = (int)(Mathf.Floor(player.transform.position.x / planeSize) * planeSize);
                playerZ = (int)(Mathf.Floor(player.transform.position.z / planeSize) * planeSize);

                for (int x = -halfTilesSizeX; x < halfTilesSizeX; x++) {
                    for (int z = -halfTilesSizeZ; z < halfTilesSizeZ; z++) {
                        auxPosition = new Vector3((x * planeSize + playerX),
                                        0,
                                        (z * planeSize + playerZ)
                            );

                        auxTileName = GenerateTileName((int)auxPosition.x, (int)auxPosition.z);

                        if (!tiles.ContainsKey(auxTileName)) {
                            auxTileObject = Instantiate(plane, auxPosition, Quaternion.identity);
                            auxTileObject.name = auxTileName;
                            tiles.Add(auxTileName, new Tile(auxTileObject, auxUpdateTime));

                        } else {
                            (tiles[auxTileName] as Tile).creationTime = auxUpdateTime;
                        }
                    }
                }

                // destroy all tiles not just created or with time updated
                // and out new tiles and tiles to be kept in a new hashtable
                auxTerrain = new Hashtable();

                foreach (Tile tls in tiles.Values) {
                    if (tls.creationTime != auxUpdateTime) {
                        //delete gameobject
                        Destroy(tls.tileGameObject);

                    } else {
                        auxTerrain.Add(tls.tileGameObject.name, tls);
                    }
                }

                // copy new hashtable content to the working hashtable
                tiles = auxTerrain;

                startPosition = player.transform.position;/**/
            }
        }
        #endregion


        #region Private methods
        private string GenerateTileName( int _xPosition, int _zPosition ) {
            return "Tile_" + ((int)(_xPosition)).ToString() + "_" + ((int)(_zPosition)).ToString();
        }
        #endregion


        #region Coroutines

        #endregion
    }
}