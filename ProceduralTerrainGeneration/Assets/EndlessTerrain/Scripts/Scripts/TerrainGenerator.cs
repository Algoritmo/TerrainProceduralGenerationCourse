﻿using UnityEngine;

namespace EndlessTerrainGeneration {

    public class TerrainGenerator : MonoBehaviour {
        #region Variables
        [SerializeField]
        private int heightScale = 5;
        [SerializeField]
        private float detailScale = 5.0f;
        #endregion


        #region MonoBehaviour
        private void Start() {
            Mesh mesh = GetComponent<MeshFilter>().mesh;
            Vector3[] vertices = mesh.vertices;

            for (int v = 0; v < vertices.Length; v++) {
                vertices[v].y = Mathf.PerlinNoise((vertices[v].x + transform.position.x) / detailScale,
                                                (vertices[v].z + transform.position.z) / detailScale) * heightScale;
            }

            mesh.vertices = vertices;
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            gameObject.AddComponent<MeshCollider>();
        }
        #endregion


        #region Public methods

        #endregion


        #region Private methods

        #endregion


        #region Coroutines

        #endregion
    }
}