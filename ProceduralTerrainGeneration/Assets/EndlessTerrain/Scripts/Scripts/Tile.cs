﻿using UnityEngine;

namespace EndlessTerrainGeneration {

    public class Tile {
        #region Variables
        public GameObject tileGameObject;
        public float creationTime;
        #endregion

        
        #region Constructor methods
        public Tile( GameObject _tileGameObject, float _creationTime ) {
            tileGameObject = _tileGameObject;
            creationTime = _creationTime;
        }
        #endregion


        #region Public methods

        #endregion


        #region Private methods

        #endregion


        #region Coroutines

        #endregion
    }
}