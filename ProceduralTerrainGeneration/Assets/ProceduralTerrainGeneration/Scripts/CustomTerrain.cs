﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class CustomTerrain : MonoBehaviour {
    #region Variables
    public Vector2 randomHeightRange = new Vector2( 0, .1f );
    public Texture2D heightMapImage;
    public Vector3 heightMapScale = new Vector3( 1, 1, 1 );
    public bool hasToResetTerrain = true;

    // Perlin noise variables
    public float perlinXScale = .01f;
    public float perlinYScale = .01f;

    public int perlinOffsetX = 0;
    public int perlinOffsetY = 0;

    public int perlinOctaves = 3;
    public float perlinPersistance = 8;
    public float perlinHeightScale = .09f;
    // END Perlin noise variables



    // Multiple Perlin noises
    [System.Serializable]
    public class PerlinParameters {
        public float mPerlinXScale = .01f;
        public float mPerlinYScale = .01f;
        public int mPerlinOffsetX = 0;
        public int mPerlinOffsetY = 0;
        public int mPerlinOctaves = 3;
        public float mPerlinPersistance = 8;
        public float mPerlinHeightScale = .09f;
        public bool remove = false;
    }

    public List<PerlinParameters> perlinParametersList = new List<PerlinParameters>() {
        new PerlinParameters()
    };
    // END Perlin noise variables


    // Voronoi variables
    public float voronoiFallOff = .2f;
    public float voronoiDropOff = .6f;
    public float voronoiMinHeight = .1f;
    public float voronoiMaxHeight = .5f;
    public int voronoiPeaksAmount = 5;

    public enum VoronoiType {  Linear = 0, Power = 1, Combined = 2, SinPow = 3 }
    public VoronoiType voronoiType = VoronoiType.Linear;
    // END Voronoi variables


    // Midlepoint Displacement
    public int mpdPower = 2;
    public float mpdMinHeight = .001f;
    public float mpdMaxHeight = 1.0f;
    public float mpdRoughness = 1.0f;
    // END Midlepoint Displacement


    // Smoothing
    public int smoothingTimes = 1;
    // END Smoothing


    // Splat maps
    [System.Serializable]
    public class SplatHeight {
        public Texture2D texture = null;

        public float minHeight = 0.1f;
        public float maxHeight = 0.2f;

        public float minSlope = 0;
        public float maxSlope = 1.5f;

        public Vector2 tileOffset = new Vector2(0, 0);
        public Vector2 tileSize = new Vector2(2, 2);

        public float splatOffset = 0.1f;
        public float splatNoiseXScale = 0.01f;
        public float splatNoiseYScale = 0.01f;
        public float splatNoiseScaler = 0.1f;

        public bool remove = false;
    }

    public List<SplatHeight> splatHeights = new List<SplatHeight>() {
        new SplatHeight()
    };
    // END Splat maps


    // Vegetation
    public int maximumTrees = 5000;
    public int treeSpacing = 5;

    [System.Serializable]
    public class TreeInfo {
        public GameObject prefab = null;

        public float minHeight = 0;
        public float maxHeight = 1;
        public float minSlope = 0;
        public float maxSlope = 50;
        public float minScale = .5f;
        public float maxScale = 1.0f;
        public float minRotation = 0;
        public float maxRotation = 360;
        public float density = .5f;
        
        public Color colour1 = Color.white;
        public Color colour2 = Color.white;
        public Color lightColour = Color.white;
        
        public bool remove = false;
    }

    public List<TreeInfo> treeInfoList = new List<TreeInfo>() {
        new TreeInfo()
    };
    // END Vegetation


    // Details
    public int maxDetails = 100;
    public int detailSpacing = 5;

    [System.Serializable]
    public class Detail {
        public GameObject prototype = null;
        public Texture2D prototypeTexture = null;

        public float minHeight = .1f;
        public float maxHeight = .2f;

        public float minSlope = 0;
        public float maxSlope = 1;

        public float noiseSpread = .5f;
        public float overlap = .01f;
        public float feather = .05f;
        public float density = .5f;

        public Color dryColour = Color.white;
        public Color healthyColour = Color.white;

        public Vector2 heightRange = new Vector2( 1, 1 );
        public Vector2 widthRange = new Vector2( 1, 1 );

        public bool remove = false;
    }

    public List <Detail> detailList = new List<Detail>() {
        new Detail()
    };
    // END Details


    // Water
    public float waterHeight = .5f;
    public GameObject waterGO;
    public Material shorelineMaterial;
    // END Water


    public Terrain terrain;
    public TerrainData terrainData;

    public enum TagType { Tag = 0, Layer  = 1 }
    [SerializeField]
    int terrainLayer = 0;
    #endregion


    #region MonoBehaviour methods
    private void Awake() {
        SerializedObject tagManager = new SerializedObject(
                AssetDatabase.LoadAllAssetsAtPath(
                        "ProjectSettings/TagManager.asset")[0] );
        SerializedProperty tagsProp = tagManager.FindProperty("tags");
        AddTag( tagsProp, "Terrain", TagType.Tag );
        AddTag( tagsProp, "Cloud", TagType.Tag );
        AddTag( tagsProp, "Shore", TagType.Tag );        

        // apply tag changes to tag database
        tagManager.ApplyModifiedProperties();

        SerializedProperty layerProp = tagManager.FindProperty("layers");
        terrainLayer = AddTag( layerProp, "Terrain", TagType.Layer );
        tagManager.ApplyModifiedProperties();


        // take this object
        gameObject.tag = "Terrain";
        gameObject.layer = terrainLayer;
    }

    private void OnEnable() {
        Debug.Log( "Initialising CustomTerrain" );
        terrain = GetComponent<Terrain>();
        terrainData = Terrain.activeTerrain.terrainData;
    }
    #endregion


    #region Public methods
    public void RandomTerrain() {
        float[,] heightMap = GetTerrainVertices();

        for ( int x = 0; x < terrainData.heightmapWidth; x++ ) {
            for (int z = 0; z < terrainData.heightmapHeight; z++ ) {
                heightMap[x, z] += UnityEngine.Random.Range( randomHeightRange.x, randomHeightRange.y );
            }
        }

        terrainData.SetHeights( 0, 0, heightMap );
    }

    public void ResetTerrain() {
        float[,] heightMap = new float[ terrainData.heightmapWidth, terrainData.heightmapHeight ];

        for (int x = 0; x < terrainData.heightmapWidth; x++) {
            for (int z = 0; z < terrainData.heightmapHeight; z++) {
                heightMap[x, z] = 0;
            }
        }

        terrainData.SetHeights(0, 0, heightMap);
    }

    public void LoadTexture () {
        float[,] heightMap = GetTerrainVertices();

        for ( int x = 0; x < terrainData.heightmapWidth; x++ ) {
            for( int z = 0; z < terrainData.heightmapHeight; z++ ) {
                heightMap[x,z] += heightMapImage.GetPixel(
                        (int) ( x * heightMapScale.x ), (int) ( z * heightMapScale.z ))
                        .grayscale * heightMapScale.y;
            }
        }
        terrainData.SetHeights( 0, 0, heightMap );
    }

    public void GenerateRandomPerlin() {
        perlinOffsetX = UnityEngine.Random.Range(0, 10000);
        perlinOffsetY = UnityEngine.Random.Range(0, 10000);
        Perlin();
    }

    public void Perlin () {
        float[,] heightMap = GetTerrainVertices();

        /*
        // First example
        for ( int y = 0; y < terrainData.heightmapHeight; y++ ) {
            for ( int x = 0; x < terrainData.heightmapWidth; x++ ) {
                heightMap[x, y] = Mathf.PerlinNoise( ( x + perlinOffsetX ) * perlinXScale,
                                                        ( y + perlinOffsetY ) * perlinYScale );
            }
        }/**/

        // Added Brownian Movment
        for ( int y = 0; y < terrainData.heightmapHeight; y++ ) {
            for ( int x = 0; x <terrainData.heightmapWidth; x++ ) {
                heightMap[x, y] += TerrainGenerationUtils.fBM(
                            ( x + perlinOffsetX ) * perlinXScale,
                            ( y + perlinOffsetY ) * perlinYScale,
                            perlinOctaves, perlinPersistance )
                            * perlinHeightScale;
            }
        }

        terrainData.SetHeights( 0, 0, heightMap );
    }
    
    public void MultiplePerlinTerrain () {
        float [,] heightMap = GetTerrainVertices();
        for( int  y = 0; y < terrainData.heightmapHeight; y++ ) {
            for( int x = 0; x < terrainData.heightmapWidth; x++ ) {
                foreach( PerlinParameters perlinParameter in perlinParametersList ) {
                    heightMap[x, y] += TerrainGenerationUtils.fBM(
                            (x + perlinOffsetX) * perlinXScale,
                            (y + perlinOffsetY) * perlinYScale,
                            perlinOctaves, perlinPersistance)
                            * perlinHeightScale;
                }
            }
        }

        terrainData.SetHeights( 0, 0, heightMap );
    }
    
    public void AddPerlinParameter() {
        perlinParametersList.Add( new PerlinParameters() );
    }

    public void RemovePerlinParameter() {
        List<PerlinParameters> keptPerlinParameters = new List<PerlinParameters>();
        for( int  i = 0; i < perlinParametersList.Count; i++ ) {
            if( !perlinParametersList[i].remove ) {
                keptPerlinParameters.Add( perlinParametersList[i] );
            }
        }

        if ( keptPerlinParameters.Count == 0 ) { // don't want to keep any
            keptPerlinParameters.Add( perlinParametersList[0] ); // add at least one, can't be empty
        }

        perlinParametersList = keptPerlinParameters;
    }
    
    public void GenerateVoronoiTesselation() {
        float[,] heightMap = GetTerrainVertices();

        float auxDistanceToPeak;
        float auxHeight;

        for (int i = 0; i < voronoiPeaksAmount; i++) {
            // getting a random seed
            int xPeak = UnityEngine.Random.Range(0, terrainData.heightmapWidth);
            int yPeak = UnityEngine.Random.Range(0, terrainData.heightmapHeight);
            float peakHeight = UnityEngine.Random.Range( voronoiMinHeight, voronoiMaxHeight );

            if (heightMap[xPeak, yPeak] < peakHeight)
                heightMap[xPeak, yPeak] = peakHeight;
            else
                continue;


            // setting heights for near vertices
            Vector2 peakLocation = new Vector2(xPeak, yPeak);

            float maxDistance = Vector2.Distance(new Vector2(0, 0), new Vector2(
                        terrainData.heightmapWidth, terrainData.heightmapHeight));
            
            for (int y = 0; y < terrainData.heightmapHeight; y++) {
                for (int x = 0; x < terrainData.heightmapWidth; x++) {
                    if (!(x == xPeak && y == yPeak)) {
                        auxDistanceToPeak = Vector2.Distance(peakLocation, new Vector2(x, y)) / maxDistance;

                        if (voronoiType == VoronoiType.Combined) {
                            auxHeight = peakHeight - auxDistanceToPeak * voronoiFallOff
                                - Mathf.Pow(auxDistanceToPeak, voronoiDropOff); // combined

                        } else if (voronoiType == VoronoiType.Power) {
                            auxHeight = peakHeight - Mathf.Pow(auxDistanceToPeak, voronoiDropOff) * voronoiFallOff;

                        } else if (voronoiType == VoronoiType.SinPow) {
                            auxHeight = peakHeight - Mathf.Pow(auxDistanceToPeak * 3, voronoiFallOff)
                                - Mathf.Sin(auxDistanceToPeak * Mathf.PI) / voronoiDropOff; 

                        } else {
                            auxHeight = peakHeight - auxDistanceToPeak * voronoiFallOff;
                        }

                        if ( heightMap[x, y] < auxHeight )
                            heightMap[x, y] = auxHeight;
                    }
                }
            }
        }

        terrainData.SetHeights( 0, 0, heightMap );
    }
    
    public void GenerateMidpointDisplacement() {
        float[,] heightMap = GetTerrainVertices();
        int width = terrainData.heightmapWidth - 1;
        int squareSize = width;
        float minHeight = mpdMinHeight;
        float maxHeight = mpdMaxHeight;
        /*
        float height = (float) squareSize / 2.0f * .01f;
        float roughness = 2.0f;*/
        float heightDampener = (float) Mathf.Pow( mpdPower, -1 * mpdRoughness);

        int cornerX, cornerY;
        int midX, midY;
        int pmidXL, pmidXR, pmidYU, pmidYD;
        /*
        heightMap[0, 0] = UnityEngine.Random.Range( 0f, .2f );
        heightMap[0, terrainData.heightmapHeight - 2] = UnityEngine.Random.Range( 0f, .2f );
        heightMap[terrainData.heightmapWidth - 2, 0] = UnityEngine.Random.Range( 0f, .2f );
        heightMap[terrainData.heightmapWidth - 2, terrainData.heightmapHeight - 2] = UnityEngine.Random.Range(0f, .2f);
        */

        while (squareSize > 0) {
            for (int x = 0; x < width; x += squareSize) {
                for (int y = 0; y < width; y += squareSize) {
                    cornerX = (x + squareSize);
                    cornerY = (y + squareSize);

                    midX = (int)(x + squareSize / 2.0f);
                    midY = (int)(y + squareSize / 2.0f);

                    heightMap[midX, midY] = (float)((heightMap[x, y] +
                                                     heightMap[cornerX, y] +
                                                     heightMap[x, cornerY] +
                                                     heightMap[cornerX, cornerY]) 
                                                     / 4.0f + UnityEngine.Random.Range( minHeight, maxHeight ) );
                }
            }

            for (int x = 0; x < width; x += squareSize) {
                for (int y = 0; y < width; y += squareSize) {
                    cornerX = (x + squareSize);
                    cornerY = (y + squareSize);

                    midX = (int) (x + squareSize / 2.0f);
                    midY = (int) (y + squareSize / 2.0f);

                    pmidXR = (int) ( midX + squareSize );
                    pmidYU = (int) ( midY + squareSize );
                    pmidXL = (int) ( midX - squareSize );
                    pmidYD = (int) ( midY - squareSize );

                    if( pmidXL <= 0     ||  pmidYD <= 0
                            ||  pmidXR >= width - 1     ||  pmidYU >= width - 1 )
                        continue;

                    // Calculates the square value for the bottom side
                    heightMap[midX, y] = (float) (( heightMap[midX, midY]
                                        + heightMap[x, y]
                                        + heightMap[midX, pmidYD]
                                        + heightMap[cornerX, y]) / 4.0f
                                        + UnityEngine.Random.Range( minHeight, maxHeight ) );

                    // Calculate the square value for the top side
                    heightMap[midX, cornerY] = (float)((heightMap[x, cornerY]
                                        + heightMap[midX, midY]
                                        + heightMap[cornerX, cornerY]
                                        + heightMap[midX, pmidYU]) / 4.0f
                                        + UnityEngine.Random.Range( minHeight, maxHeight ) );

                    // Calculate the square value for the left side
                    heightMap[x, midY] = (float)((heightMap[x, y]
                                        + heightMap[pmidXL, midY]
                                        + heightMap[x, cornerY]
                                        + heightMap[midX, midY]) / 4.0f
                                        + UnityEngine.Random.Range( minHeight, maxHeight ) );

                    // Calculate the square value for the right side
                    heightMap[cornerX, midY] = (float)((heightMap[midX, y]
                                        + heightMap[midX, midY]
                                        + heightMap[cornerX, cornerY]
                                        + heightMap[pmidXR, midY]) / 4.0f
                                        + UnityEngine.Random.Range( minHeight, maxHeight ) );

                }
            }

            squareSize = (int)(squareSize / 2.0f);
            minHeight *= heightDampener;
            maxHeight *= heightDampener;
        }

        terrainData.SetHeights(0, 0, heightMap);
    }
    
    public void Smooth() {
        // there's no point in smoothing after reset
        float [,] heightMap = terrainData.GetHeights(0, 0,
                terrainData.heightmapWidth, terrainData.heightmapHeight);
        float smoothProgress = 0;
        EditorUtility.DisplayProgressBar("Smoothing Terrain", "Progress", smoothProgress);

        for (int i = 0; i < smoothingTimes; i++) {
            for (int y = 0; y < terrainData.heightmapHeight; y++) {
                for (int x = 0; x < terrainData.heightmapWidth; x++) {
                    float avgHeight = heightMap[x, y];
                    List<Vector2> neightbours = GenerateNeighbours(new Vector2(x, y),
                                                terrainData.heightmapWidth,
                                                terrainData.heightmapHeight);

                    foreach (Vector2 neighbour in neightbours) {
                        avgHeight += heightMap[(int)neighbour.x, (int)neighbour.y];
                    }

                    heightMap[x, y] = avgHeight / ((float)neightbours.Count + 1);
                }
            }

            smoothProgress++;
            EditorUtility.DisplayProgressBar(
                "Smoothing Terrain", "Progress", smoothProgress/smoothingTimes);
        }

        terrainData.SetHeights( 0, 0, heightMap );
        EditorUtility.ClearProgressBar();
    }
   
    
    public void AddSplatHeight() {
        splatHeights.Add( new SplatHeight() );
    }

    public void RemoveSplatHeight () {
        List <SplatHeight> keptSplatHeights = new List<SplatHeight>();
        for ( int i = 0; i < splatHeights.Count; i++ ) {
            if( !splatHeights[i].remove ) {
                keptSplatHeights.Add( splatHeights[i] );
            }
        }

        if ( keptSplatHeights.Count == 0 ) {    // don't want to keep any
            keptSplatHeights.Add( splatHeights[0] );    // add at least one
        }

        splatHeights = keptSplatHeights;
    }

    public void SplatMaps() {
        TerrainLayer[] newSplatPrototypes = new TerrainLayer[splatHeights.Count];
        int splatIndex = 0;

        foreach( SplatHeight splatHeight in splatHeights ) {
            newSplatPrototypes[splatIndex] = new TerrainLayer();
            newSplatPrototypes[splatIndex].diffuseTexture = splatHeight.texture;
            newSplatPrototypes[splatIndex].tileOffset = splatHeight.tileOffset;
            newSplatPrototypes[splatIndex].tileSize = splatHeight.tileSize;
            newSplatPrototypes[splatIndex].diffuseTexture.Apply( true );
            string path = "Assets/New Terrain Layer " + splatIndex + ".terrainlayer";
            AssetDatabase.CreateAsset( newSplatPrototypes[splatIndex], path );

            splatIndex++;
            Selection.activeObject = this.gameObject;
        }

        terrainData.terrainLayers = newSplatPrototypes;

        
        float [,] heightMap = terrainData.GetHeights( 0, 0, terrainData.heightmapWidth,
                                terrainData.heightmapHeight );
        float[,,] splatMapData = new float[terrainData.alphamapWidth,
                            terrainData.alphamapHeight, terrainData.alphamapLayers];
        //float offset = splatOffset;
        float offset;
        float thisHeightStart;
        float thisHeightStop;
        float[] splat;
        float noise;
        float steepness;

        for ( int y = 0; y < terrainData.alphamapHeight; y++ ) {
            for ( int x = 0; x < terrainData.alphamapWidth; x++ ) {
                splat = new float[ terrainData.alphamapLayers ];

                for( int i = 0; i < splatHeights.Count; i++ ) {
                    noise = Mathf.PerlinNoise(x * splatHeights[i].splatNoiseXScale, y 
                        * splatHeights[i].splatNoiseYScale) * splatHeights[i].splatNoiseScaler;
                    offset = splatHeights[i].splatOffset + noise;

                    thisHeightStart = splatHeights[i].minHeight - offset;
                    thisHeightStop = splatHeights[i].maxHeight + offset;
                    /*
                    steepness = GetSteepness( heightMap, x, y,
                                    terrainData.heightmapWidth,
                                    terrainData.heightmapHeight );/**/

                    steepness = terrainData.GetSteepness( y / (float) terrainData.alphamapHeight,
                                                          x / (float) terrainData.alphamapWidth );

                    if ( ( heightMap[x, y] >= thisHeightStart
                            &&  heightMap[x,y] <= thisHeightStop
                            &&  steepness >= splatHeights[i].minSlope 
                            &&  steepness <= splatHeights[i].maxSlope ) ) {
                        splat[i] = 1;
                    } else {
                        splat[i] = 0;
                    }/**/

                    //NormalizeVector( splat );

                    for( int j = 0; j < splatHeights.Count; j++ ) {
                        splatMapData[x, y, j] = splat[j];
                    }
                }
            }
        }

        terrainData.SetAlphamaps(0, 0, splatMapData);
    }
    
    
    public void AddTree() {
        treeInfoList.Add( new TreeInfo() );
    }

    public void RemoveTree() {
        List<TreeInfo> keptTrees = new List<TreeInfo>();
        for (int i = 0; i < treeInfoList.Count; i++) {
            if (!treeInfoList[i].remove) {
                keptTrees.Add(treeInfoList[i]);
            }
        }

        if (keptTrees.Count == 0) { // don't want to keep any
            keptTrees.Add(treeInfoList[0]); // add at least one, can't be empty
        }

        treeInfoList = keptTrees;
    }
    
    public void PlantVegetation() {
        TreePrototype[] newTreePrototypes = new TreePrototype[treeInfoList.Count];
        int treeIndex = 0;

        foreach( TreeInfo tree in treeInfoList ) {
            newTreePrototypes[treeIndex] = new TreePrototype();
            newTreePrototypes[treeIndex].prefab = tree.prefab;
            treeIndex++;
        }

        terrainData.treePrototypes = newTreePrototypes;


        float thisHeight;
        float steepness;

        List <TreeInstance> allVegetation = new List<TreeInstance> ();
        for ( int z = 0; z < terrainData.size.z; z+= treeSpacing ) {
            for (int x = 0; x < terrainData.size.x; x += treeSpacing) {
                for( int treeP = 0; treeP  < terrainData.treePrototypes.Length; treeP++ ) {
                    if (UnityEngine.Random.Range(0.0f, 1.0f) > treeInfoList[treeP].density)
                        break;

                    thisHeight = terrainData.GetHeight( x, z ) / terrainData.size.y;
                    steepness = terrainData.GetSteepness( x / ( float) terrainData.size.x,
                                                          z / (float)terrainData.size.z );
                    TreeInstance instance = new TreeInstance();
                    

                    if (    thisHeight >= treeInfoList[treeP].minHeight
                            && thisHeight <= treeInfoList[treeP].maxHeight
                            && steepness >= treeInfoList[treeP].minSlope
                            && steepness <= treeInfoList[treeP].maxSlope ) {

                        /*
                        instance.position = new Vector3( x / terrainData.size.x,
                                                         thisHeight,
                                                         z / terrainData.size.z );/**/
                        
                        instance.position = new Vector3((x + UnityEngine.Random.Range(-5f, 5f)) / terrainData.size.x,
                                                         terrainData.GetHeight(x, z) / terrainData.size.y,
                                                         (z + UnityEngine.Random.Range(-5f, 5f)) / terrainData.size.z);
                        
                        
                        Vector3 treeWorlPos = new Vector3( instance.position.x * terrainData.size.x,
                                                            instance.position.y * terrainData.size.y,
                                                            instance.position.z * terrainData.size.z )
                                              + transform.position;

                        RaycastHit hit;
                        int layerMask = 1 << terrainLayer;
                        if( Physics.Raycast( treeWorlPos + new Vector3( 0, 10, 0 ), -Vector3.up, out hit, 100, layerMask)
                                    ||  Physics.Raycast(treeWorlPos - new Vector3(0, 10, 0), Vector3.up, out hit, 100, layerMask) ) {
                            float treeHeight = ( hit.point.y - transform.position.y ) / terrainData.size.y;
                            instance.position = new Vector3( instance.position.x,
                                                            treeHeight,
                                                            instance.position.z );
                            instance.rotation = UnityEngine.Random.Range( treeInfoList[treeP].minRotation,
                                                                          treeInfoList[treeP].maxRotation );
                            instance.prototypeIndex = treeP;
                            instance.color = Color.Lerp( treeInfoList[treeP].colour1,
                                                        treeInfoList[treeP].colour2,
                                                        UnityEngine.Random.Range( .0f, 1.0f ) );
                            instance.lightmapColor = treeInfoList[treeP].lightColour;
                            float calculatedScale = UnityEngine.Random.Range(
                                                   treeInfoList[treeP].minScale, treeInfoList[treeP].maxScale);
                            instance.heightScale = calculatedScale;
                            instance.widthScale = calculatedScale;

                            allVegetation.Add(instance);
                            if (allVegetation.Count >= maximumTrees)
                                goto TREESDONE;
                        }

                        // fix for scaling different terrain sizes
                        //------------ADD THIS
                        instance.position = new Vector3(instance.position.x * terrainData.size.x / terrainData.alphamapWidth,
                                                        instance.position.y,
                                                        instance.position.z * terrainData.size.z / terrainData.alphamapHeight);
                        //------------ADD THIS
                    }
                }
            }
        }

        TREESDONE:
            terrainData.treeInstances = allVegetation.ToArray();
    }
    
    
    public void AddDetail() {
        detailList.Add( new Detail() );
    }

    public void RemoveDetail() {
        List<Detail> keptDetail = new List<Detail>();
        for (int i = 0; i < detailList.Count; i++) {
            if (!detailList[i].remove) {
                keptDetail.Add(detailList[i]);
            }
        }

        if (keptDetail.Count == 0) { // don't want to keep any
            keptDetail.Add(detailList[0]); // add at least one, can't be empty
        }

        detailList = keptDetail;
    }

    public void Details() {
        DetailPrototype[] detailPrototypes = new DetailPrototype[detailList.Count];
        int detailIndex = 0;

        foreach( Detail detail in detailList ) {
            detailPrototypes[detailIndex] = new DetailPrototype();
            detailPrototypes[detailIndex].prototype = detail.prototype;
            detailPrototypes[detailIndex].prototypeTexture = detail.prototypeTexture;
            detailPrototypes[detailIndex].healthyColor = detail.healthyColour;
            detailPrototypes[detailIndex].dryColor = detail.dryColour;
            detailPrototypes[detailIndex].minHeight = detail.heightRange.x;
            detailPrototypes[detailIndex].maxHeight = detail.heightRange.y;
            detailPrototypes[detailIndex].minWidth = detail.widthRange.x;
            detailPrototypes[detailIndex].maxWidth = detail.widthRange.y;
            detailPrototypes[detailIndex].noiseSpread = detail.noiseSpread;



            if ( detailPrototypes[detailIndex].prototype ) {
                detailPrototypes[detailIndex].usePrototypeMesh = true;
                detailPrototypes[detailIndex].renderMode = DetailRenderMode.VertexLit;

            } else {
                detailPrototypes[detailIndex].usePrototypeMesh = false;
                detailPrototypes[detailIndex].renderMode = DetailRenderMode.GrassBillboard;
            }

            detailIndex++;
        }

        terrainData.detailPrototypes = detailPrototypes;

        float[,] heightMap = terrainData.GetHeights(0, 0, terrainData.heightmapWidth,
                                                    terrainData.heightmapHeight);
        int[,] detailMap;             
                       
        for ( int i = 0; i < terrainData.detailPrototypes.Length; i++ ) {
            detailMap = new int[terrainData.detailWidth, terrainData.detailHeight];

            for ( int y = 0; y < terrainData.detailHeight; y += detailSpacing ) {
                for( int x = 0; x < terrainData.detailWidth; x+= detailSpacing ) {

                    if (UnityEngine.Random.Range(0.0f, 1.0f) > detailList[i].density)
                        continue;

                    int xHM = (int) ( x / (float) terrainData.detailWidth * terrainData.heightmapWidth);
                    int yHM = (int) ( y / (float) terrainData.detailHeight * terrainData.heightmapHeight);

                    float thisNoise = TerrainGenerationUtils.Map( Mathf.PerlinNoise(
                                                                    x * detailList[i].feather,
                                                                    y * detailList[i].feather),
                                                                    0, 1, .5f, 1 );
                    float thisHeightStart = detailList[i].minHeight * thisNoise
                                            - detailList[i].overlap * thisNoise;

                    float nextHeightStart = detailList[i].maxHeight * thisNoise
                                            + detailList[i].overlap * thisNoise;

                    float thisHeight = heightMap[yHM, xHM];
                    float steepness = terrainData.GetSteepness( xHM / (float) terrainData.size.x,
                                                                yHM / (float) terrainData.size.z );

                    if( (thisHeight >= thisHeightStart  &&  thisHeight <= nextHeightStart )/*
                                    && thisHeight <= detailList[i].minHeight
                                    && thisHeight <= detailList[i].maxHeight/**/
                                    &&  steepness >= detailList[i].minSlope
                                    &&  steepness <= detailList[i].maxSlope ) {
                        detailMap[y, x] = 1;
                    }
                }
            }

            terrainData.SetDetailLayer( 0, 0, i, detailMap );
        }
    }
    


    public void AddWater() {
        GameObject water = GameObject.Find("water");
        if( !water ) {
            water = Instantiate(waterGO, transform.position, transform.rotation );
            water.name = "water";
        }

        water.transform.position = transform.position + new Vector3( terrainData.size.x / 2,
                                                            waterHeight * terrainData.size.y,
                                                            terrainData.size.z / 2 );
        water.transform.localScale = new Vector3( terrainData.size.x, 1, terrainData.size.z );
    }

    public void AddShoreline() {
        float[,] heightMap = terrainData.GetHeights( 0, 0,
                                                     terrainData.heightmapWidth,
                                                     terrainData.heightmapHeight );

        GameObject auxGameObject = GameObject.Find("Shoreline");
        if( auxGameObject )
            DestroyImmediate( auxGameObject );
        GameObject quads = new GameObject("Shoreline");

        for( int y = 0; y < terrainData.heightmapHeight; y++ ) {
            for( int x = 0; x < terrainData.heightmapWidth; x++ ) {
                // find spot on shore
                Vector2 thisLocation = new Vector2( x, y );
                List<Vector2> neighbours = GenerateNeighbours( thisLocation,
                                                                terrainData.heightmapWidth,
                                                                terrainData.heightmapHeight );

                foreach( Vector2 neighbour in neighbours ) {
                    if( heightMap[x, y] < waterHeight
                            && heightMap[(int) neighbour.x, (int) neighbour.y] > waterHeight ) {

                        GameObject auxQuad = GameObject.CreatePrimitive( PrimitiveType.Quad );
                        auxQuad.transform.localScale *= 10.0f;
                        auxQuad.transform.position = transform.position
                                                + new Vector3( y / (float) terrainData.heightmapHeight
                                                                                * terrainData.size.z,
                                                                waterHeight * terrainData.size.y,
                                                                x / (float) terrainData.heightmapWidth
                                                                                    * terrainData.size.x );
                        auxQuad.transform.LookAt( new Vector3( neighbour.y / (float) terrainData.heightmapHeight
                                                                        * terrainData.size.x,
                                                               waterHeight * terrainData.size.y,
                                                               neighbour.x / (float) terrainData.heightmapWidth
                                                                        * terrainData.size.z) );

                        auxQuad.transform.Rotate(90, 0, 0);
                        auxQuad.tag = "Shore";
                        auxQuad.transform.parent = quads.transform;
                    }
                }
            }
        }

        GameObject[] shoreQuads = GameObject.FindGameObjectsWithTag("Shore");
        MeshFilter[] meshFilters = new MeshFilter[shoreQuads.Length];

        for( int m = 0; m < shoreQuads.Length; m++ ) {
            meshFilters[m] = shoreQuads[m].GetComponent<MeshFilter>();
        }

        CombineInstance[] combine = new CombineInstance[meshFilters.Length];
        int i = 0;

        while( i < meshFilters.Length ) {
            combine[i].mesh = meshFilters[i].sharedMesh;
            combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
            meshFilters[i].gameObject.SetActive( false );

            i++;
        }


        GameObject currentShoreLine = GameObject.Find("ShoreLine");
        if ( currentShoreLine ) {
            DestroyImmediate( currentShoreLine );
        }

        GameObject shoreLine2 = new GameObject("ShoreLine2");
        shoreLine2.name = "ShoreLine2";
        shoreLine2.AddComponent<WaveAnimation>();
        shoreLine2.transform.position = transform.position;
        shoreLine2.transform.rotation = transform.rotation;

        MeshFilter thisMeshFilter = shoreLine2.AddComponent<MeshFilter>();
        thisMeshFilter.mesh = new Mesh();
        shoreLine2.GetComponent<MeshFilter>().sharedMesh.CombineMeshes( combine );

        MeshRenderer renderer = shoreLine2.AddComponent<MeshRenderer>();
        renderer.sharedMaterial = shorelineMaterial;

        for( int sQ = 0; sQ <shoreQuads.Length; sQ++ ) {
            DestroyImmediate( shoreQuads[sQ] );
        }
    }
    
    
    
    public void Isolate() {
        float[,] heightMap = terrainData.GetHeights(
                                            0, 0,
                                            terrainData.heightmapWidth,
                                            terrainData.heightmapHeight);

        float auxDistance;
        float auxResult;
        for (int y = 0; y < terrainData.heightmapHeight; y++) {
            for (int x = 0; x < terrainData.heightmapWidth; x++) {
                auxDistance = Vector2.Distance(new Vector2(
                                    terrainData.heightmapWidth / 2,
                                    terrainData.heightmapHeight / 2),
                                    new Vector2(x, y) );
                if ( auxDistance > (terrainData.heightmapWidth / 2) ) {
                    heightMap[x, y] = 0;

                } else {
                    auxResult = (1f - (auxDistance / (terrainData.heightmapWidth / 2f) )) * 2f;
                    if ( auxResult < 1f )
                        heightMap[x, y] = heightMap[x, y] *  auxResult;
                }
            }
        }


        terrainData.SetHeights(0, 0, heightMap);
    }
    #endregion


    #region Private methods
    private int AddTag( SerializedProperty _tagsProp, string _newTag, TagType tagType ) {
        bool isFound = false;

        // ensure that the tag doesn't already exists
        SerializedProperty auxProperty;

        for ( int i = 0; i < _tagsProp.arraySize; i++ ) {
            auxProperty = _tagsProp.GetArrayElementAtIndex( i );
            if (auxProperty.stringValue.Equals(_newTag)) {
                isFound = true;
                return i;
            }
        }
        
        // add your new tag
        if ( !isFound   &&  tagType == TagType.Tag ) {
            _tagsProp.InsertArrayElementAtIndex(0);
            SerializedProperty newTagProperty = _tagsProp.GetArrayElementAtIndex( 0 );
            newTagProperty.stringValue = _newTag;

        } else if ( !isFound    &&  tagType == TagType.Layer ) {
            for ( int j = 8; j < _tagsProp.arraySize; j++ ) {
                SerializedProperty newLayer = _tagsProp.GetArrayElementAtIndex(j);
                // add layer in the next empty slot
                if( newLayer.stringValue == "" ) {
                    Debug.Log( "Adding new layer:" + _newTag );
                    newLayer.stringValue = _newTag;
                    return j;
                }
            }
        }

        return -1;
    }
    
    private float[,] GetTerrainVertices() {
        if ( hasToResetTerrain ) 
            return new float[terrainData.heightmapWidth, terrainData.heightmapHeight];

        return terrainData.GetHeights(0, 0, terrainData.heightmapWidth,
                                            terrainData.heightmapHeight);
    }

    private List<Vector2> GenerateNeighbours(Vector2 pos, int width, int height) {
        List<Vector2> neighbours = new List<Vector2>();
        for (int y = -1; y < 2; y++) {
            for (int x = -1; x < 2; x++) {
                if (!(x == 0 && y == 0)) {
                    Vector2 nPos = new Vector2(Mathf.Clamp(pos.x + x, 0, width - 1),
                                                Mathf.Clamp(pos.y + y, 0, height - 1));
                    if (!neighbours.Contains(nPos))
                        neighbours.Add(nPos);
                }
            }
        }
        return neighbours;
    }
    
    private void NormalizeVector( float[] _floatVector ) {
        float total = 1;

        for ( int i = 0; i < _floatVector.Length; i++ ) {
            total += _floatVector[i];
        }
        
        for ( int i = 0; i < _floatVector.Length; i++ ) {
            _floatVector[i] /= total;
        }
    }
    
    private float GetSteepness ( float[,] _heightMap, int _x, int _y, int _width, int _height ) {
        float height = _heightMap[_x, _y];
        int nX = _x + 1;
        int nY = _y + 1;

        // if on the upper edge of the map find gradient by going backward
        if( nX >_width - 1 )
            nX = _x - 1;
        if( nY > _height - 1 )
            nY = _y - 1;

        float dX = _heightMap[nX, _y] - height;
        float dY = _heightMap[_x, nY] - height;
        Vector2 gradient = new Vector2( dX, dY );

        return gradient.magnitude;  // steep
    }
    #endregion
}
