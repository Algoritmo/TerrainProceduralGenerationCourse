﻿using UnityEngine;
using UnityEditor;
using EditorGUITable;

[CustomEditor(typeof(CustomTerrain))]
[CanEditMultipleObjects]
public class CustomTerrainEditor : Editor {
    #region Variables
    // properties
    private SerializedProperty randomHeightRange;
    private SerializedProperty heightMapScale;
    private SerializedProperty heightMapImage;
    private SerializedProperty perlinXScale;
    private SerializedProperty perlinYScale;
    private SerializedProperty perlinOffsetX;
    private SerializedProperty perlinOffsetY;
    private SerializedProperty perlinOctaves;
    private SerializedProperty perlinPersistance;
    private SerializedProperty perlinHeightScale;
    private SerializedProperty hasToResetTerrain;

    private GUITableState perlinParameterTable;
    private SerializedProperty perlinParametersList;

    private SerializedProperty voronoiFallOff;
    private SerializedProperty voronoiDropOff;
    private SerializedProperty voronoiMinHeight;
    private SerializedProperty voronoiMaxHeight;
    private SerializedProperty voronoiPeaksAmount;
    private SerializedProperty voronoiType;

    private SerializedProperty mpdPower;
    private SerializedProperty mpdMinHeight;
    private SerializedProperty mpdMaxHeight;
    private SerializedProperty mpdRoughness;

    private SerializedProperty smoothingTimes;


    private GUITableState splatMapsTable;
    private SerializedProperty splatHeights;
    
    private SerializedProperty splatOffset;
    private SerializedProperty xNoiseScale;
    private SerializedProperty yNoiseScale;
    private SerializedProperty noiseScaler;


    private GUITableState treeInfoTable;
    private SerializedProperty treeInfoList;

    private SerializedProperty maximumTrees;
    private SerializedProperty treeSpacing;


    private SerializedProperty maxDetails;
    private SerializedProperty detailSpacing;
    private GUITableState detailTable;
    private SerializedProperty detailList;

    private SerializedProperty waterHeight;
    private SerializedProperty waterGO;
    private SerializedProperty shorelineMaterial;
    // END properties


    // fold outs
    private bool showRandom = false;
    private bool showLoadHeights = false;
    private bool showLoadPerlin = false;
    private bool showMultiplePerlin = false;
    private bool showVoronoiTesselation = false;
    private bool showMidpointDisplacement = false;
    private bool showSmoothing = false;
    private bool showSplatMap = false;
    private bool showGenerateHeightMap = false;
    private bool showVegetation = false;
    private bool showDetail = false;
    private bool showWater = false;
    private bool showIsland = false;
    // END fold outs

    private Texture2D heightMapTexture;
    private Vector2 scrollPosition;
    #endregion


    #region MonoBehaviour methods
    private void OnEnable() {
        heightMapTexture = new Texture2D( 513, 513, TextureFormat.ARGB32, false );

        randomHeightRange = serializedObject.FindProperty( "randomHeightRange" );
        heightMapScale = serializedObject.FindProperty( "heightMapScale" );
        heightMapImage = serializedObject.FindProperty( "heightMapImage" );
        perlinXScale = serializedObject.FindProperty( "perlinXScale" );
        perlinYScale = serializedObject.FindProperty( "perlinYScale" );
        perlinOffsetX = serializedObject.FindProperty( "perlinOffsetX" );
        perlinOffsetY = serializedObject.FindProperty( "perlinOffsetY" );
        perlinOctaves = serializedObject.FindProperty( "perlinOctaves" );
        perlinPersistance = serializedObject.FindProperty( "perlinPersistance" );
        perlinHeightScale = serializedObject.FindProperty( "perlinHeightScale" );
        hasToResetTerrain = serializedObject.FindProperty( "hasToResetTerrain" );
        perlinParameterTable = new GUITableState( "perlinParameterTable" );
        perlinParametersList = serializedObject.FindProperty( "perlinParametersList" );

        voronoiFallOff = serializedObject.FindProperty( "voronoiFallOff");
        voronoiDropOff = serializedObject.FindProperty( "voronoiDropOff");
        voronoiMinHeight = serializedObject.FindProperty( "voronoiMinHeight");
        voronoiMaxHeight = serializedObject.FindProperty( "voronoiMaxHeight");
        voronoiPeaksAmount = serializedObject.FindProperty( "voronoiPeaksAmount");
        voronoiType = serializedObject.FindProperty( "voronoiType" );

        mpdPower = serializedObject.FindProperty("mpdPower");
        mpdMinHeight = serializedObject.FindProperty("mpdMinHeight");
        mpdMaxHeight = serializedObject.FindProperty("mpdMaxHeight");
        mpdRoughness = serializedObject.FindProperty("mpdRoughness");

        smoothingTimes = serializedObject.FindProperty("smoothingTimes");

        splatMapsTable = new GUITableState("splatMapsTable");
        splatHeights = serializedObject.FindProperty("splatHeights");

        treeInfoTable = new GUITableState("treeInfoTable");
        treeInfoList = serializedObject.FindProperty("treeInfoList");
        maximumTrees = serializedObject.FindProperty("maximumTrees");
        treeSpacing = serializedObject.FindProperty("treeSpacing");

        maxDetails = serializedObject.FindProperty("maxDetails");
        detailSpacing = serializedObject.FindProperty("detailSpacing");
        detailTable = new GUITableState("detailTable");
        detailList = serializedObject.FindProperty("detailList");

        waterHeight = serializedObject.FindProperty("waterHeight");
        waterGO = serializedObject.FindProperty("waterGO");
        shorelineMaterial = serializedObject.FindProperty("shorelineMaterial");
    }
    #endregion


    #region Public methods
    public override void OnInspectorGUI() {
        serializedObject.Update();

        CustomTerrain terrain = (CustomTerrain) target;

        // Scrollbar begining code
        Rect rect = EditorGUILayout.BeginVertical();
        scrollPosition = EditorGUILayout.BeginScrollView( scrollPosition, GUILayout.Width( rect.width),
            GUILayout.Height( rect.height ) );
        EditorGUI.indentLevel++;
        // END Scrollbar begining code

        EditorGUILayout.PropertyField( hasToResetTerrain );

        // Generate new terrain button
        showRandom = EditorGUILayout.Foldout( showRandom, "Random" );
        if ( showRandom ) {
            EditorGUILayout.LabelField( "", GUI.skin.horizontalSlider );
            GUILayout.Label( "Set heights between random values.", EditorStyles.boldLabel );
            EditorGUILayout.PropertyField( randomHeightRange );

            if ( GUILayout.Button("Random Heights") ) {
                terrain.RandomTerrain();
            }
        }


        // Load Map Height from Image Button
        showLoadHeights = EditorGUILayout.Foldout( showLoadHeights, "Load Heights" );
        if ( showLoadHeights ) {
            EditorGUILayout.LabelField( "", GUI.skin.horizontalSlider );
            GUILayout.Label( "Load Heights From Texture", EditorStyles.boldLabel );
            EditorGUILayout.PropertyField( heightMapImage );
            EditorGUILayout.PropertyField( heightMapScale );
            if ( GUILayout.Button( "Load Texture") ) {
                terrain.LoadTexture();
            }
        }


        // Generate Random with Perlin noise Button
        showLoadPerlin = EditorGUILayout.Foldout( showLoadPerlin, "Perlin Noise" );
        if ( showLoadPerlin ) {
            EditorGUILayout.LabelField( "", GUI.skin.horizontalSlider );
            GUILayout.Label("Generate with Perlin Noise", EditorStyles.boldLabel);
            EditorGUILayout.Slider( perlinXScale, 0, 1, new GUIContent( "Perlin X Scale" ) );
            EditorGUILayout.Slider( perlinYScale, 0, 1, new GUIContent( "Perlin Y Scale" ) );
            EditorGUILayout.IntSlider( perlinOffsetX, 0, 10000, new GUIContent( "Offset X" ) );
            EditorGUILayout.IntSlider( perlinOffsetY, 0, 10000, new GUIContent( "Offset Y" ) );
            EditorGUILayout.IntSlider( perlinOctaves, 1, 10, new GUIContent( "Octaves" ) );
            EditorGUILayout.Slider( perlinPersistance, .1f, 1, new GUIContent( "Persistance" ) );
            EditorGUILayout.Slider( perlinHeightScale, 0, 1, new GUIContent( "Height scale" ) );
            if ( GUILayout.Button("Generate") ) {
                terrain.Perlin();
            } else if ( GUILayout.Button("Generate random") ) {
                terrain.GenerateRandomPerlin();
            }
        }


        // Multiple Perlin
        showMultiplePerlin = EditorGUILayout.Foldout( showMultiplePerlin, "Multiple Perlin noise" );
        if ( showMultiplePerlin ) {
            EditorGUILayout.LabelField( "", GUI.skin.horizontalSlider );
            GUILayout.Label( "Multiple Perlin noise", EditorStyles.boldLabel );
            perlinParameterTable = GUITableLayout.DrawTable( perlinParameterTable,
                            serializedObject.FindProperty( "perlinParametersList" ) );
            GUILayout.Space( 20 );

            EditorGUILayout.BeginHorizontal();
            if ( GUILayout.Button("+") ) {
                terrain.AddPerlinParameter();

            } else if ( GUILayout.Button("-") ) {
                terrain.RemovePerlinParameter();
            }
            EditorGUILayout.EndHorizontal();

            if ( GUILayout.Button("Apply Multiple Perlin") ) {
                terrain.MultiplePerlinTerrain();
            }
        }


        // Voronoi Tesseleation
        showVoronoiTesselation = EditorGUILayout.Foldout( showVoronoiTesselation, "Voronoi Tesselation" );
        if ( showVoronoiTesselation ) {
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider );
            GUILayout.Label( "Voronoi Tesselation", EditorStyles.boldLabel );

            EditorGUILayout.IntSlider( voronoiPeaksAmount, 1, 10, new GUIContent("Peak count") );
            EditorGUILayout.Slider( voronoiFallOff, 0, 10, new GUIContent("Falloff") );
            EditorGUILayout.Slider( voronoiDropOff, 0, 10, new GUIContent("Dropoff") );
            EditorGUILayout.Slider( voronoiMinHeight, 0, 1, new GUIContent("Minimum height") );
            EditorGUILayout.Slider( voronoiMaxHeight, 0, 1, new GUIContent("Maximum height") );
            EditorGUILayout.PropertyField( voronoiType );

            if ( GUILayout.Button("Generate a seed") ) {
                terrain.GenerateVoronoiTesselation();
            }
        }
        // END Voronoir Tesseleation


        // Midpoint displacement
        showMidpointDisplacement = EditorGUILayout.Foldout( showMidpointDisplacement,  "Midpoint Displacement" );
        if ( showMidpointDisplacement ) {
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            GUILayout.Label("Midpoint Displacement", EditorStyles.boldLabel);

            EditorGUILayout.Slider(mpdMinHeight, 0, 1, new GUIContent("Minimum height"));
            EditorGUILayout.Slider(mpdMaxHeight, 0, 1, new GUIContent("Maximum height"));
            EditorGUILayout.Slider(mpdRoughness, 0, 10, new GUIContent("Roughness"));
            EditorGUILayout.IntSlider(mpdPower, 0, 10, new GUIContent("Dampler Power"));

            if (GUILayout.Button("Generate")) {
                terrain.GenerateMidpointDisplacement();
            }
        }
        // END Midpoint displacement


        // Smoothing
        showSmoothing = EditorGUILayout.Foldout( showSmoothing,  "Smoothing" );
        if ( showSmoothing ) {
            EditorGUILayout.LabelField( "", GUI.skin.horizontalSlider );
            EditorGUILayout.IntSlider(smoothingTimes, 1, 10, new GUIContent("Smoothing times"));
            if (GUILayout.Button("Smooth")) {
                terrain.Smooth();
            }
        }
        //END Smoothing


        // Splat maps
        showSplatMap = EditorGUILayout.Foldout( showSplatMap, "Splat maps");
        if( showSplatMap ) {
            EditorGUILayout.LabelField( "", GUI.skin.horizontalSlider );
            GUILayout.Label( "Splat Maps", EditorStyles.boldLabel );
            splatMapsTable = GUITableLayout.DrawTable( splatMapsTable,
                            serializedObject.FindProperty("splatHeights") );
            GUILayout.Space( 20 );

            EditorGUILayout.BeginHorizontal();
            if( GUILayout.Button( "+" ) ) {
                terrain.AddSplatHeight();

            } else if ( GUILayout.Button( "-" ) ) {
                terrain.RemoveSplatHeight();
            }
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button( "Apply Splat Maps" )) {
                terrain.SplatMaps();
            }
        }
        //END Splat maps


        // Vegetation
        showVegetation = EditorGUILayout.Foldout(showVegetation, "Vegetation");
        if ( showVegetation ) {
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            GUILayout.Label("Vegetation", EditorStyles.boldLabel);

            EditorGUILayout.IntSlider( maximumTrees, 0, 10000, new GUIContent("Maximum trees") );
            EditorGUILayout.IntSlider( treeSpacing, 0, 100, new GUIContent("Tree spacing"));

            treeInfoTable = GUITableLayout.DrawTable(treeInfoTable,
                            serializedObject.FindProperty("treeInfoList"));
            GUILayout.Space(20);

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("+")) {
                terrain.AddTree();

            } else if (GUILayout.Button("-")) {
                terrain.RemoveTree();
            }
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Plant Vegetation")) {
                terrain.PlantVegetation();
            }
        }
        // END Vegetation


        // Detail
        showDetail = EditorGUILayout.Foldout(showDetail, "Detail");
        if( showDetail ) {
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            GUILayout.Label("Detail", EditorStyles.boldLabel);
                        
            EditorGUILayout.IntSlider( maxDetails, 0, 10000, new GUIContent("Maximum details") );
            EditorGUILayout.IntSlider( detailSpacing, 0, 100, new GUIContent("Detail spacing") );

            terrain.GetComponent<Terrain>().detailObjectDistance = maxDetails.intValue;

            detailTable = GUITableLayout.DrawTable( detailTable,
                            serializedObject.FindProperty("detailList") );
            GUILayout.Space(20);

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("+")) {
                terrain.AddDetail();

            } else if (GUILayout.Button("-")) {
                terrain.RemoveDetail();
            }
            EditorGUILayout.EndHorizontal();

            if (GUILayout.Button("Apply")) {
                terrain.Details();
            }
        }
        // END Detail


        // Water
        showWater = EditorGUILayout.Foldout(showWater, "Water");
        if( showWater ) {
            EditorGUILayout.LabelField( "", GUI.skin.horizontalSlider );
            GUILayout.Label( "Water", EditorStyles.boldLabel );
            EditorGUILayout.Slider(waterHeight, 0, 1, new GUIContent("Water height") );
            EditorGUILayout.PropertyField( waterGO );

            if ( GUILayout.Button( "Add Water") ) {
                terrain.AddWater();

            }

            EditorGUILayout.PropertyField( shorelineMaterial );

            if ( GUILayout.Button("Generate shoreline") ) {
                terrain.AddShoreline();
            }
        }
        // END Water



        // Island
        showIsland = EditorGUILayout.Foldout(showIsland, "Island");
        if (showIsland) {
            EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            GUILayout.Label("Island", EditorStyles.boldLabel);

            if (GUILayout.Button("Isolate")) {
                terrain.Isolate();
            }
        }
        // END Island




        // Reset terrain button
        EditorGUILayout.LabelField( "", GUI.skin.horizontalSlider );
        if ( GUILayout.Button("Reset Terrain") ) {
            terrain.ResetTerrain();
        }

        

        // Generate Height Map
        showGenerateHeightMap = EditorGUILayout.Foldout( showGenerateHeightMap, "Generate Height Map" );
        if ( showGenerateHeightMap ) {
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            int heightMapSize = (int) (EditorGUIUtility.currentViewWidth - 100 );
            GUILayout.Label( heightMapTexture, GUILayout.Width( heightMapSize ),
                    GUILayout.Height( heightMapSize ) );
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            if( GUILayout.Button("Refresh", GUILayout.Width( heightMapSize ) ) ) {
                float[,] heightMap = terrain.terrainData.GetHeights( 0, 0,
                            terrain.terrainData.heightmapWidth,
                            terrain.terrainData.heightmapHeight );

                for ( int y = 0; y < terrain.terrainData.heightmapHeight; y++ ) {
                    for (int x = 0; x < terrain.terrainData.heightmapWidth; x++) {
                        heightMapTexture.SetPixel( x, y, new Color( heightMap[x, y],
                                                                    heightMap[x, y],
                                                                    heightMap[x, y],
                                                                    1 ) );
                    }
                }

                heightMapTexture.Apply();
            }

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();
        }
        // END Generate Height Map
        



        // Scrollbar ending code
        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();
        // END Scrollbar ending code

        serializedObject.ApplyModifiedProperties();
    }
    #endregion


    #region Private methods

    #endregion
}
