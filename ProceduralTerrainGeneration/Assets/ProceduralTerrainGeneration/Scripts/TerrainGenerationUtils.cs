﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TerrainGenerationUtils {
    #region Public methods
    public static float fBM( float _x, float _y, int _octave, float _persistance ) {
        float total = 0;
        float frequency = 1;
        float amplitude = 1;
        float maxValue = 0;

        for ( int i = 0; i < _octave; i++ ) {
            total += Mathf.PerlinNoise( _x * frequency, _y * frequency ) * amplitude;
            maxValue += amplitude;
            amplitude *= _persistance;
            frequency *= 2;
        }
        
        return total / maxValue;
    }

    public static float Map ( float _value, float _originalMin, float _originalMax,
                    float _targetMin, float _targetMax ) {
        return ( _value - _originalMin ) * ( _targetMax - _targetMin ) 
                / (_originalMax - _originalMin ) + _targetMin;
    }
    #endregion
}
