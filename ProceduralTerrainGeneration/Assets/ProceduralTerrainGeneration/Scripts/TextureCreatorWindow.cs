﻿using UnityEditor;
using UnityEngine;
using System.IO;


public class TextureCreatorWindow : EditorWindow {
    #region Variables
    private string fileName = "myProceduralTexture";

    private float perlinXScale;
    private float perlinYScale;

    private int perlinOctaves;

    private float perlinPersistance;
    private float perlinHeightScale;

    private int perlinOffsetX;
    private int perlinOffsetY;

    private Texture2D pTexture;

    private bool alphaToggle = false;
    private bool seamlessToggle = false;
    private bool mapToggle = false;

    private float brightness = .5f;
    private float contrast = .5f;
    #endregion


    #region MonoBehaviour methods
    private void OnEnable() {
        pTexture = new Texture2D( 513, 513, TextureFormat.ARGB32, false );
    }

    private void OnGUI() {
        GUILayout.Label("Settings", EditorStyles.boldLabel);
        fileName = EditorGUILayout.TextField("Texture name", fileName);

        int wSize = (int) (EditorGUIUtility.currentViewWidth - 100 );


        perlinXScale = EditorGUILayout.Slider("X Scale", perlinXScale, 0, .1f);
        perlinYScale = EditorGUILayout.Slider("Y Scale", perlinYScale, 0, .1f);

        perlinOctaves = EditorGUILayout.IntSlider("Octaves", perlinOctaves, 1, 10);
        perlinPersistance = EditorGUILayout.Slider("Persistance", perlinPersistance, 1, 10);
        perlinHeightScale = EditorGUILayout.Slider("Height Scale", perlinHeightScale, 0f, 1f);

        perlinOffsetX = EditorGUILayout.IntSlider("X Offset", perlinOffsetX, 0, 10000 );
        perlinOffsetY = EditorGUILayout.IntSlider("Y Offset", perlinOffsetY, 0, 10000);

        brightness = EditorGUILayout.Slider("Brightness", brightness, 0f, 2f);
        contrast = EditorGUILayout.Slider("Contrast", contrast, 0f, 2f);

        alphaToggle = EditorGUILayout.Toggle("Alpha?", alphaToggle);
        mapToggle = EditorGUILayout.Toggle("Map?", mapToggle);
        seamlessToggle = EditorGUILayout.Toggle("Seamless?", seamlessToggle);

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        float minColour = 1;
        float maxColour = 0;
        if( GUILayout.Button( "Generate", GUILayout.Width( wSize ))) {
            int width = 513;
            int height = 513;
            float pValue;
            Color pixelColour = Color.white;

            for ( int y = 0; y < height; y++ ) {
                for ( int x = 0; x < width; x++ ) {
                    if (seamlessToggle) {
                        float u = (float) x / (float) width;
                        float v = (float) y / (float) height;

                        float noise00 = TerrainGenerationUtils.fBM( (x + perlinOffsetX) * perlinXScale,
                                            (y + perlinOffsetY) * perlinYScale,
                                            perlinOctaves,
                                            perlinPersistance) * perlinHeightScale;

                        float noise01 = TerrainGenerationUtils.fBM( ( x + perlinOffsetX) * perlinXScale,
                                            ( y + perlinOffsetY + height) * perlinYScale,
                                            perlinOctaves,
                                            perlinPersistance ) * perlinHeightScale;

                        float noise10 = TerrainGenerationUtils.fBM( ( x + perlinOffsetX + width) * perlinXScale,
                                            (y + perlinOffsetY ) * perlinYScale,
                                            perlinOctaves,
                                            perlinPersistance ) * perlinHeightScale;

                        float noise11 = TerrainGenerationUtils.fBM((x + perlinOffsetX + width) * perlinXScale,
                                            (y + perlinOffsetY + height) * perlinYScale,
                                            perlinOctaves,
                                            perlinPersistance) * perlinHeightScale;

                        float noiseTotal = u * v * noise00 
                                            + u * ( 1 - v ) * noise01
                                            + ( 1 - u ) * v * noise10
                                            + ( 1 - u ) * ( 1 - v ) * noise11;

                        float value = (int) ( 256 * noiseTotal) + 50;
                        float r = Mathf.Clamp( (int)noise00, 0, 255 );
                        float g = Mathf.Clamp( value, 0, 255 );
                        float b = Mathf.Clamp( value + 50, 0, 255 );
                        float a = Mathf.Clamp( value + 100, 0, 255 );

                        pValue = ( r + g + b) / ( 3 * 255.0f );

                    } else {
                        pValue = TerrainGenerationUtils.fBM((x + perlinOffsetX) * perlinXScale,
                                                        (y + perlinOffsetY) * perlinYScale,
                                                        perlinOctaves,
                                                        perlinPersistance * perlinHeightScale);
                    }

                    float auxColorValue = contrast * ( pValue - .5f ) + .5f * brightness;

                    if ( maxColour < auxColorValue )
                        maxColour = auxColorValue;
                    if (minColour > auxColorValue)
                        minColour = auxColorValue;

                    pixelColour = new Color( auxColorValue,
                                auxColorValue, auxColorValue,
                                alphaToggle ? auxColorValue : 1 );
                    pTexture.SetPixel( x, y, pixelColour );
                }
            }

            if ( mapToggle ) {
                for( int y = 0; y < height; y++ ) {
                    for( int x = 0; x < width; x++ ) {
                        pixelColour = pTexture.GetPixel( x, y );
                        float colourValue = pixelColour.r;
                        colourValue = TerrainGenerationUtils.Map( colourValue, minColour, maxColour, 0, 1 );
                        pixelColour.r = colourValue;
                        pixelColour.g = colourValue;
                        pixelColour.b = colourValue;
                        pTexture.SetPixel( x, y, pixelColour );
                    }
                }
            }

            pTexture.Apply( false, false );
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUILayout.Label( pTexture, GUILayout.Width( wSize ), GUILayout.Height( wSize ) );
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if( GUILayout.Button( "Save", GUILayout.Width( wSize ) ) ) {
            byte[] bytes = pTexture.EncodeToPNG();
            System.IO.Directory.CreateDirectory( Application.dataPath + "/SavedTextures" );
            File.WriteAllBytes( Application.dataPath + "/SavedTextures/" + fileName + ".png", bytes );
        }
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }
    #endregion


    #region Public methods
    [MenuItem("Window/TextureCreatorWindow")]
    public static void ShowWindow() {
        EditorWindow.GetWindow(typeof( TextureCreatorWindow ));
    }
    #endregion


    #region Private methods

    #endregion
}
